Contexte du projet
Brief :

​

À partir de la maquette réalisée précédemment (ou celle d’un⋅e collègue), réalisez l’intégration CSS de la maquette en HTML 5 et CSS 3.

​

Utilisez des balises sémantiques appropriées, ne mettez pas des div partout :

    Les balises sémantiques en HTML5

​

N’utilisez pas de framework CSS, vous devez utiliser les Flexbox (et optionnellement les CSS Grid) :

    https://developer.mozilla.org/fr/docs/Learn/CSS/CSS_layout/Flexbox

    https://yoksel.github.io/flex-cheatsheet/

    https://developer.mozilla.org/fr/docs/Web/CSS/grid

    https://yoksel.github.io/grid-cheatsheet/

​

Créez des variables CSS pour les polices de caractères et les couleurs.

Pour en savoir plus, vous pouvez utiliser cette ressource :

    https://developer.mozilla.org/fr/docs/Web/CSS/Using_CSS_custom_properties

​

Nommez vos classes CSS en utilisant la méthode BEM :

    Méthodologie BEM pour le CSS

​

Pour savoir comment nommer vos variables CSS, vous pouvez lire cet article :

    Proper Naming of Color Variables

​

Essayez de donner des noms génériques, décrivez la variable par sa fonction, pas par sa couleur et l’élément ciblé, ex :

    --bg-color-primary plutôt que --div-red
    --font-title plutôt que --h1-montserrat

​

Pour les padding/margin, utilisez l’unité de mesure rem : 1rem équivaut à 16px, 0.5rem à 8px.Il ne devrait jamais être nécessaire de dépasser 4rem.

    Unité de mesure CSS : REM

​
Ce que vous devez faire :

​

    Créer la structure HTML5 de votre maquette : dans un premier temps, ne faites pas de CSS, créez uniquement les éléments HTML avec les bonnes balises sémantiques
    Créer les variables CSS générales : les couleurs, les polices (et optionnellement les padding/margin en rem)
    Ajoutez des classes à vos éléments en respectant la convention BEM
    Stylez vos éléments en utilisant des Flexbox (et optionnellement des CSS Grid)
    Réalisez l’intégration complète d’une seule page
    Partagez votre code sur Gitlab

​
Aller plus loin :

​

    Réalisez l’ensemble de l’intégration de l’application
    Ajoutez du JavaScript et de la manipulation du DOM

Modalités pédagogiques

En autonomie, sur une journée
Livrables

Dépôt Gitlab contenant au moins une page HTML et une page CSS